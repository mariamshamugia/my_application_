package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity2 : AppCompatActivity() {


    lateinit var  email: TextView
    lateinit var  password : TextView
    lateinit var firstname: TextView
    lateinit var  lastname: TextView
    lateinit var username: TextView
    lateinit var register: Button
    lateinit var registered: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        email.findViewById<TextView>(R.id.mail)
        password.findViewById<TextView>(R.id.pass)
        firstname.findViewById<TextView>(R.id.firstname)
        lastname.findViewById<TextView>(R.id.lastname)
        username.findViewById<TextView>(R.id.username)
        register.findViewById<Button>(R.id.register)
        registered.findViewById<Button>(R.id.registered)

        val email = intent.extras?.getString("email", "")
        val password = intent.extras?.getString("password","")

    }
}