package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.net.PasswordAuthentication

class MainActivity : AppCompatActivity() {

    lateinit var  email: TextView
    lateinit var  password : TextView
    lateinit var signup: TextView
    lateinit var loginbutton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        signup = findViewById(R.id.signup)


        signup.setOnClickListener {
            val email = email.text.toString()
            val password = password.text.toString()


            val intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("email", email)
            intent.putExtra("password", password)
            startActivity(intent)

            val login = findViewById<Button>(R.id.login)

            login.setOnClickListener {
                val toast = Toast.makeText(this, "Hello Maro", Toast.LENGTH_SHORT)
                toast.show()
            }




        }

    }
}